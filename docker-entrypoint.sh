#!/bin/bash
if [ ! -z "$NEWRELIC_KEY" ]; then
	echo "Newrelic key is set. Installing newrelic key"
	echo "newrelic.license=\"$NEWRELIC_KEY\"" > /etc/php/7.0/fpm/conf.d/newrelic.ini
else
	echo "Newrelic key is not set. Skipping"
fi

exec "$@"