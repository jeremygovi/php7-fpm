FROM debian:jessie

# Set the timezone.
RUN echo "Europe/Paris" > /etc/timezone
RUN dpkg-reconfigure -f noninteractive tzdata

RUN apt-get update \ 
	&& apt-get -y install wget ca-certificates \
	&& rm -rf /var/lib/apt/lists/*

RUN echo "deb http://packages.dotdeb.org jessie all" > /etc/apt/sources.list.d/dotdeb.list \
    && wget -O- https://www.dotdeb.org/dotdeb.gpg | apt-key add - \
    && apt-get update \ 
	&& apt-get -y install php7.0 \
	php7.0-fpm \
	php7.0-mysql  \
	php7.0-curl  \
	php7.0-json  \
	php7.0-gd  \
	php7.0-mcrypt  \
	php7.0-msgpack  \
	php7.0-memcached  \
	php7.0-intl  \
	php7.0-gmp  \
	php7.0-geoip  \
	php7.0-mbstring  \
	php7.0-xml  \
	php7.0-zip \
	&& rm -rf /var/lib/apt/lists/*

# Newrelic part
RUN echo newrelic-php5 newrelic-php5/license-key string "" | debconf-set-selections
RUN echo newrelic-php5 newrelic-php5/application-name string "PHP7 Application" | debconf-set-selections
RUN wget -O - https://download.newrelic.com/548C16BF.gpg | apt-key add -
RUN echo "deb http://apt.newrelic.com/debian/ newrelic non-free" > /etc/apt/sources.list.d/newrelic.list
RUN apt-get update && apt-get install -y newrelic-php5 && newrelic-install install && apt-get clean && rm -rf /var/lib/apt/lists/*

COPY config/php-fpm.conf /etc/php/7.0/fpm/
COPY config/www.conf /etc/php/7.0/fpm/pool.d/

RUN mkdir -p /var/log/php-fpm/ && chmod a+rw -R /var/log/php-fpm
VOLUME /var/log/php-fpm/

COPY docker-entrypoint.sh /usr/local/bin/
ENTRYPOINT ["docker-entrypoint.sh"]
CMD php-fpm7.0 -F
EXPOSE 9000